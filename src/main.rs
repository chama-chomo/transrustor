extern crate reqwest;
extern crate select;

use std::{env, vec};

use select::document::Document;
use select::predicate::{Class, Name};

fn main() {
    let args: String = process_arguments();

    let site_result: Document = get_data(
        "https://slovnik.aktuality.sk/preklad/anglicko-slovensky/?q=",
        args.to_owned().as_str(),
    )
    .unwrap();

    let mut translated = find_translations(site_result);

    println!("=== Translations retrieved for '{}' ===", args);
    while let Some(pop) = translated.pop() {
        println!("{}", pop)
    }
}

/// Process args given on the command line
fn process_arguments() -> String {
    let mut args: Vec<String> = env::args().collect();
    args.remove(0); // remove basename
    args.join(" ")
}

/// Get data from the remote site
fn get_data(url: &str, word: &str) -> Result<Document, reqwest::Error> {
    let full_url = format!("{}{}", url, word);
    let resp = reqwest::blocking::get(&full_url)?;
    assert!(resp.status().is_success());

    let document = Document::from(resp.text_with_charset("utf-8").unwrap().as_str());

    Ok(document)
}

/// Return all translations found in the data retrieved from the remote site
fn find_translations(site_result: Document) -> vec::Vec<String> {
    let mut words = vec![];

    let table = site_result.find(Class("p")).next().unwrap();
    let result_table = table.find(Class("do"));

    for node in result_table {
        let result = node.find(Name("span")).next().unwrap().text();
        words.push(result);
    }

    words.reverse(); // reverse order
    words
}
