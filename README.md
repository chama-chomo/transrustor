# Transrustor

Terminal tool for providing translations between Slovak and Eng

Can be integrated with rofi

```
#!/bin/sh

WORD=$(rofi -dmenu -p 'Insert WORD:' -location 6 -width 100) 
transrustor $WORD | rofi -dmenu  
```

